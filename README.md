# Bowling Score Calculator

This is a console based Bowling Score Calculator. It reads an input file containing a record of each roll on a given game.

The input file should follow these rules:

- Each line represents a player and a chance with the subsequent number of pins
  knocked down.
- An 'F' indicates a foul on that chance and no pins knocked down (identical for
  scoring to a roll of 0).
- The rows are tab-separated.

## Usage

- Download the code from this repository

    `$ git clone https://kmilovm91722@bitbucket.org/kmilovm91722/bowling-score-calculator.git`
    
- Build using Maven

    `$ cd bowling-score-calculator/`
    
    `$ mvn clean package`

- Execute it
    
    `$ java -jar target/bowling-score-calculator.jar <PATH_TO_INPUT_FILE>`
        