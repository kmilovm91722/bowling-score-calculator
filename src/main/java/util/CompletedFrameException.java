package util;

public class CompletedFrameException extends BowlingScoreCalculatorException {

    public CompletedFrameException(String message) {
        super(message);
    }
}
