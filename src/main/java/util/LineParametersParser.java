package util;

import model.frame.Roll;
import org.apache.commons.lang3.StringUtils;

public class LineParametersParser {

    private String playerName;
    private Roll roll;

    public LineParametersParser(String paramsText) {
        if (StringUtils.isBlank(paramsText)) {
            throw new LineFormatException("A line cannot be empty");
        }
        String[] params = paramsText.split("\t");
        if (params.length != 2) {
            throw new LineFormatException("A line had a wrong number of params.");
        }

        playerName = params[0];
        roll = new Roll(params[1]);
    }

    public String getPlayerName() {
        return playerName;
    }

    public Roll getRoll() {
        return roll;
    }
}
