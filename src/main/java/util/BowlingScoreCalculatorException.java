package util;

public class BowlingScoreCalculatorException extends RuntimeException {

    public BowlingScoreCalculatorException(String message) {
        super(message);
    }
}
