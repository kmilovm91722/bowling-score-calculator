package util;

public class WrongPlayerTurnException extends BowlingScoreCalculatorException {

    private static final String DEFAULT_MESSAGE = "A player took his turn at the wrong time. ";

    public WrongPlayerTurnException(String message) {
        super(DEFAULT_MESSAGE + message);
    }

    public WrongPlayerTurnException() {
        super(DEFAULT_MESSAGE);
    }
}
