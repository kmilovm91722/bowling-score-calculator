package util;

import model.game.GameConstants;

import java.io.PrintStream;

public class PrintUtils {

    private static PrintStream output = System.out;

    public static void setOutput(PrintStream output) {
        PrintUtils.output = output;
    }

    public static PrintStream getOutput() {
        return output;
    }

    public static void printSeparator() {
        getOutput().print(GameConstants.SEPARATOR);
    }

    public static void printNewLine() {
        getOutput().print("\n");
    }
}
