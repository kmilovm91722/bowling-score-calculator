package util;

public class LineFormatException extends BowlingScoreCalculatorException {

    public LineFormatException(String message) {
        super(message);
    }
}
