package util;

public class InvalidRollException extends BowlingScoreCalculatorException {

    public InvalidRollException(String message) {
        super(message);
    }
}
