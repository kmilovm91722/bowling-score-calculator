package app;

import model.game.Game;
import util.BowlingScoreCalculatorException;
import util.LineParametersParser;
import util.PrintUtils;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;


/**
 * Entry point of the application. Reads the input file, create a games and start registering rolls by each line.
 */
public class BowlingScoreCalculatorApplication {

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            System.out.println("Usage: java -jar bowling-score-calculator.jar <INPUT_FILE_PATH>");
            return;
        }
        launchBowlingScoreCalculator(args[0], System.out);
    }

    public static void launchBowlingScoreCalculator(String inputFilePath, PrintStream output) {
        PrintUtils.setOutput(output);
        Game game = new Game();
        try (Stream<String> stream = Files.lines(Paths.get(inputFilePath))) {

            stream.forEach(line -> {
                LineParametersParser parser = new LineParametersParser(line);
                game.registerRoll(parser.getPlayerName(), parser.getRoll());
            });
            game.calculateScores();
            game.print();

        } catch (BowlingScoreCalculatorException | IOException e) {
            PrintUtils.getOutput().println("Error: " + e.getMessage());
        }
    }

}