package model.player;

import model.Printable;
import model.frame.Roll;
import model.frame.Frame;
import model.frame.LastFrame;
import model.game.GameConstants;
import util.InvalidRollException;
import util.PrintUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Represents a Player, manages his plays and scores.
 */
public class Player implements Printable {

    private Frame[] frames;
    private int currentFrameIndex;
    private List<Roll> rolls;
    private int rollsIndex;

    public Player() {
        this.frames = new Frame[GameConstants.NUMBER_OF_FRAMES];
        this.currentFrameIndex = 0;
        this.rolls = new ArrayList<>();
        rollsIndex = 0;
    }

    /**
     *
     * @param roll
     * @return true if the frame is completed by the results of this roll.
     * @throws InvalidRollException if the player frames are all already completed.
     */
    public boolean addRoll(Roll roll) throws InvalidRollException {
        if (currentFrameIndex > GameConstants.NUMBER_OF_FRAMES - 1) {
            throw new InvalidRollException("All possible frames have been already completed for a player.");
        }
        roll.setIndex(rollsIndex);
        rolls.add(roll);
        rollsIndex++;
        Frame frame = frames[currentFrameIndex];
        boolean isLastFrame = currentFrameIndex == GameConstants.NUMBER_OF_FRAMES - 1;
        if (frame == null) {
            if (isLastFrame) {
                frame = new LastFrame();
            } else {
                frame = new Frame();
            }
        }

        frame.addRoll(roll);

        frames[currentFrameIndex] = frame;

        if (frame.isCompleted()) {
            currentFrameIndex++;
            return true;
        }
        return false;
    }

    /**
     * Calculates the scores for this player doing it so for each of his frames.
     */
    public void calculateScores() {
        int previousFrameScore = 0;
        for (int i = 0; i < frames.length; i++) {
            int lastRollIndex = frames[i].getLastRollIndex();
            if (i < frames.length - 1) {
                frames[i].calculateScore(previousFrameScore, rolls.get(lastRollIndex + 1).getPinFalls(), rolls.get(lastRollIndex + 2).getPinFalls());
            } else {
                frames[i].calculateScore(previousFrameScore, 0, 0);
            }
            previousFrameScore = frames[i].getScore();
        }
    }

    private void printFrames() {
        PrintUtils.getOutput().print("Pinfalls");
        Stream.of(frames).forEach(Frame::print);
    }

    private void printScores() {
        PrintUtils.getOutput().print("Score");
        Stream.of(this.frames).forEach(frame -> {
            PrintUtils.printSeparator();
            PrintUtils.printSeparator();
            PrintUtils.getOutput().print(frame.getScore());
        });
    }

    @Override
    public void print() {
        printFrames();
        PrintUtils.printNewLine();
        printScores();
        PrintUtils.printNewLine();
    }
}