package model.frame;

import model.Printable;
import util.LineFormatException;
import util.PrintUtils;

/**
 * Roll. Manages the amount of pinFalls and the representation and value of a Foul. It also has an index to be used
 * by players and later by frames to calculate scores.
 */
public class Roll implements Printable {

    private static final String FOUL_TOKEN = "F";

    private Integer pinFalls;
    private Integer index;
    private String text;

    public Roll(String text) {
        this.text = text;
    }

    /**
     *
     * @return The amount of pinFalls represented by this roll.
     * @throws LineFormatException if the roll text cannot be parsed.
     */
    public int getPinFalls() throws LineFormatException {
        if (this.pinFalls == null) {
            if (FOUL_TOKEN.equals(this.text)) {
                this.pinFalls = 0;
            } else {
                try {
                    this.pinFalls = Integer.valueOf(text);
                } catch (NumberFormatException e) {
                    throw new LineFormatException(e.getMessage());
                }
            }
        }
        return pinFalls.intValue();
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    @Override
    public void print() {
        PrintUtils.getOutput().print(this.text);
    }
}
