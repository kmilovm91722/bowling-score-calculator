package model.frame;

import model.game.GameConstants;
import model.Printable;
import util.CompletedFrameException;
import util.InvalidRollException;
import util.PrintUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Frame representation, manages its rolls and the logic to calculate its score.
 */
public class Frame implements Printable {

    protected List<Roll> rolls;
    private Integer score;

    public Frame() {
        this.rolls = new ArrayList<>();
    }

    /**
     * Adds a roll to this frame. It also validates if this frame is still valid after adding the roll; if not,
     * it would throw an InvalidRollException.
     * @param roll
     * @throws CompletedFrameException
     * @throws InvalidRollException
     */
    public void addRoll(Roll roll) throws CompletedFrameException, InvalidRollException {
        if (this.isCompleted()) {
            throw new CompletedFrameException("All possible rolls were already played on a frame. " +
                    "A player took his chance at the wrong time.");
        }
        rolls.add(roll);
        this.validateFramePinFalls();
    }

    /**
     * Checks if this Frame is completed. A Frame is completed if it has no more chances to do a roll.
     * @return
     */
    public boolean isCompleted() {
        return this.getFramePinFalls() == GameConstants.NUMBER_OF_PINS || rolls.size() == 2;
    }

    /**
     * Validates if the Frame pin falls are valid. The number of pin falls in a given Frame cannot be greater than
     * the number of available pins.
     * @throws InvalidRollException
     */
    protected void validateFramePinFalls() throws InvalidRollException {
        if (this.getFramePinFalls() > GameConstants.NUMBER_OF_PINS) {
            throw new InvalidRollException("More than " + GameConstants.NUMBER_OF_PINS + " cannot be knocked on the same frame.");
        }
    }

    /**
     *
     * @return The sum of pin falls in this frame rolls.
     */
    public int getFramePinFalls() {
        return rolls.stream().mapToInt(Roll::getPinFalls).sum();
    }

    /**
     * Calculates the frame scored based on the previous frame score and the next two rolls (Which may or not be used
     * depending if the current frame has a strike or a spare.)
     * @param previousFrameScore
     * @param nextFirstRoll
     * @param nextSecondRoll
     * @return The frame score
     */
    public int calculateScore(int previousFrameScore, int nextFirstRoll, int nextSecondRoll) {
        int score = previousFrameScore + this.getFramePinFalls();
        if (isStrike()) {
            score += nextFirstRoll + nextSecondRoll;
        } else if (isSpare()) {
            score += nextFirstRoll;
        }
        this.score = score;
        return score;
    }

    /**
     *
     * @return The frame score if calculateScore have been already called or null if not.
     */
    public Integer getScore() {
        return score;
    }

    /**
     * Gets the index for the last roll of this frame. This method is used to find the next two rolls independently of
     * their frames.
     * @return
     */
    public int getLastRollIndex() {
        return this.rolls.get(this.rolls.size() - 1).getIndex();
    }

    /**
     * Checks if this frame has a strike
     * @return true if this frame has a strike shot
     */
    public boolean isStrike() {
        return this.rolls.size() > 0 && this.rolls.get(0).getPinFalls() == GameConstants.NUMBER_OF_PINS;
    }

    /**
     * Checks if this frame has a spare
     * @return true if all the pins were knocked down in this frame but not in its first roll.
     */
    public boolean isSpare() {
        return this.rolls.size() == 2 && getFramePinFalls() == GameConstants.NUMBER_OF_PINS;
    }

    @Override
    public void print() {
        PrintUtils.printSeparator();
        if (isStrike()) {
            PrintUtils.printSeparator();
            PrintUtils.getOutput().print(GameConstants.STRIKE);
        } else {
            rolls.get(0).print();
            PrintUtils.printSeparator();
            if (isSpare()) {
                PrintUtils.getOutput().print(GameConstants.SPARE);
            } else {
                rolls.get(1).print();
            }
        }
    }
}
