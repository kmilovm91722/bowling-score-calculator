package model.frame;

import model.game.GameConstants;
import util.InvalidRollException;
import util.PrintUtils;

/**
 * An extension to Frame. It defines an alternative behavior for the LastFrame which may have 3 rolls instead of 2.
 */
public class LastFrame extends Frame {

    /**
     * Checks if this Frame is completed. A Last Frame is completed if it has two rolls or three if
     * it can have a third roll.
     * @return
     */
    @Override
    public boolean isCompleted() {
        return (!canHaveThirdShot() && this.rolls.size() == 2) || (canHaveThirdShot() && this.rolls.size() == 3);
    }

    /**
     * Validate if the Frame pin falls are valid. It checks if the frame can have a third shot.
     */
    @Override
    protected void validateFramePinFalls() {
        if (rolls.size() > 1 && !isStrike() && rolls.get(0).getPinFalls() + rolls.get(1).getPinFalls() > GameConstants.NUMBER_OF_PINS) {
            throw new InvalidRollException("More than the possible number of pins have been knocked on the same frame.");
        }
    }

    /**
     *
     * @return
     */
    public boolean isSpare() {
        return this.rolls.size() > 1 && this.rolls.get(0).getPinFalls() + this.rolls.get(1).getPinFalls() == GameConstants.NUMBER_OF_PINS;
    }

    /**
     * Checks if the frame have its two first rolls as strikes.
     * @return
     */
    private boolean isSecondStrike() {
        return rolls.size() >= 2 && this.isStrike() && this.rolls.get(1).getPinFalls() == GameConstants.NUMBER_OF_PINS;
    }

    /**
     * Checks if the frame can have a third roll.
     * @return
     */
    private boolean canHaveThirdShot() {
        return this.rolls.size() > 1 && this.rolls.get(0).getPinFalls() + this.rolls.get(1).getPinFalls() >= GameConstants.NUMBER_OF_PINS;
    }

    @Override
    public void print() {
        PrintUtils.printSeparator();
        if (isStrike()) {
            PrintUtils.getOutput().print(GameConstants.STRIKE);
        } else {
            rolls.get(0).print();
        }
        PrintUtils.printSeparator();
        if (isSecondStrike()) {
            PrintUtils.getOutput().print(GameConstants.STRIKE);
        } else if (isSpare()) {
            PrintUtils.getOutput().print(GameConstants.SPARE);
        } else {
            rolls.get(1).print();
        }
        if (canHaveThirdShot()) {
            PrintUtils.printSeparator();
            if (rolls.get(2).getPinFalls() == GameConstants.NUMBER_OF_PINS) {
                PrintUtils.getOutput().print(GameConstants.STRIKE);
            } else {
                rolls.get(2).print();
            }
        }
    }
}
