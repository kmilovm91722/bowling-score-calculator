package model.game;

import model.frame.Roll;
import model.player.Player;
import model.Printable;
import org.apache.commons.lang3.StringUtils;
import util.InvalidRollException;
import util.PrintUtils;
import util.WrongPlayerTurnException;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The Game. It manages the players and keeps track of the turns they should be taking based on if a
 * frame has been completed or not.
 */
public class Game implements Printable {

    private Map<String, Player> players;
    private boolean firstRound;
    private boolean frameCompleted;
    private String lastPlayerName;
    private Iterator<String> playersNamesIterator;

    public Game() {
        players = new LinkedHashMap<>();
        firstRound = true;
        frameCompleted = false;
    }

    /**
     * Register a roll in the game for the given player.
     * @param playerName
     * @param roll
     * @throws InvalidRollException If the roll params are invalid.
     * @throws WrongPlayerTurnException If a player takes a turn at the wrong time.
     */
    public void registerRoll(String playerName, Roll roll) throws InvalidRollException, WrongPlayerTurnException{
        if (StringUtils.isBlank(playerName)) {
            throw new InvalidRollException("Player name cannot be empty");
        }

        int pinFalls = roll.getPinFalls();
        if (pinFalls < 0) {
            throw new InvalidRollException("The amount of pin falls cannot be negative.");
        }

        if (pinFalls > GameConstants.NUMBER_OF_PINS) {
            throw new InvalidRollException("The amount of pin falls cannot be grater than " + GameConstants.NUMBER_OF_PINS);
        }

        Player player = players.get(playerName);

        if (player == null) {
            if (!firstRound) {
                throw new WrongPlayerTurnException("A new player was added to the game after the first round.");
            }
            player = new Player();
        } else if (frameCompleted) {
            firstRound = false;
            if (playersNamesIterator == null || !playersNamesIterator.hasNext()) {
                playersNamesIterator = players.keySet().iterator();
            }
            if (!playerName.equals(playersNamesIterator.next())) {
                throw new WrongPlayerTurnException();
            }
        } else if (players.size() > 1 && !playerName.equals(lastPlayerName)) {
            throw new WrongPlayerTurnException("A player had one more roll chance.");
        }

        frameCompleted = player.addRoll(roll);
        lastPlayerName = playerName;
        players.put(playerName, player);
    }

    /**
     * Calculate the scores for each player.
     */
    public void calculateScores() {
        players.values().forEach(Player::calculateScores);
    }

    private void printFramesHeadlines() {
        PrintUtils.getOutput().print("Frame");
        PrintUtils.printSeparator();
        PrintUtils.printSeparator();
        for (int i = 1; i <= GameConstants.NUMBER_OF_FRAMES; i++) {
            PrintUtils.getOutput().print(i);
            if (i < GameConstants.NUMBER_OF_FRAMES) {
                PrintUtils.printSeparator();
                PrintUtils.printSeparator();
            } else {
                PrintUtils.printNewLine();
            }
        }
    }

    @Override
    public void print() {
        printFramesHeadlines();
        players.forEach((name, player) -> {
            PrintUtils.getOutput().println(name);
            player.print();
        });
    }
}
