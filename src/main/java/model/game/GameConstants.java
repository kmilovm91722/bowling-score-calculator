package model.game;

public class GameConstants {

    public static final int NUMBER_OF_FRAMES = 10;
    public static final int NUMBER_OF_PINS = 10;
    public static final String SEPARATOR = "\t";
    public static final String STRIKE = "X";
    public static final String SPARE = "/";
}
