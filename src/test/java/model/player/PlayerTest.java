package model.player;

import model.frame.Roll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import util.InvalidRollException;

public class PlayerTest {

    @Test
    public void itIsNotPossibleToHaveMoreThanTheMaximumNumberOfFrames() {
        Player player = new Player();
        player.addRoll(new Roll("10"));
        player.addRoll(new Roll("10"));
        player.addRoll(new Roll("10"));
        player.addRoll(new Roll("10"));
        player.addRoll(new Roll("10"));
        player.addRoll(new Roll("10"));
        player.addRoll(new Roll("10"));
        player.addRoll(new Roll("10"));
        player.addRoll(new Roll("10"));
        player.addRoll(new Roll("10"));
        player.addRoll(new Roll("10"));
        player.addRoll(new Roll("10"));
        InvalidRollException exception = Assertions.assertThrows(InvalidRollException.class, () -> {
            player.addRoll(new Roll("10"));
        });
    }
}
