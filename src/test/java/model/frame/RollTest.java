package model.frame;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import util.CompletedFrameException;
import util.LineFormatException;

public class RollTest {

    @Test
    public void testFoulIsRecognizedAsZero() {
        Roll roll = new Roll("F");
        Assertions.assertEquals(0, roll.getPinFalls());
    }

    @Test
    public void testNumbersAreCorrectlyParsed() {
        Roll roll = new Roll("8");
        Assertions.assertEquals(8, roll.getPinFalls());
    }

    @Test
    public void testItFailsWhenPassingNotANumberAsParameter() {
        Roll roll = new Roll("8a");
        LineFormatException exception = Assertions.assertThrows(LineFormatException.class, () -> {
            roll.getPinFalls();
        });
    }
}
