package model.frame;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import util.CompletedFrameException;
import util.InvalidRollException;

public class FrameTest {

    @Test
    public void testAddRollIsNotPossibleOnACompletedFrame() {
        Frame frame = new Frame();
        CompletedFrameException exception = Assertions.assertThrows(CompletedFrameException.class, () -> {
            frame.addRoll(new Roll("4"));
            frame.addRoll(new Roll("4"));
            frame.addRoll(new Roll("4"));
        });
    }

    @Test
    public void testAddRollFailsWhenItsSumIsGreaterThanMaximumAllowed() {
        Frame frame = new Frame();
        InvalidRollException exception = Assertions.assertThrows(InvalidRollException.class, () -> {
            frame.addRoll(new Roll("6"));
            frame.addRoll(new Roll("6"));
        });
    }

    @Test
    public void testFrameIsCompletedAfterTwoRolls() {
        Frame frame = new Frame();
        frame.addRoll(new Roll("3"));
        frame.addRoll(new Roll("6"));

        Assertions.assertTrue(frame.isCompleted());
    }

    @Test
    public void testFrameIsCompletedAfterAStrike() {
        Frame frame = new Frame();
        frame.addRoll(new Roll("10"));

        Assertions.assertTrue(frame.isCompleted());
    }

    @Test
    public void testFrameDoesNotAllowAnotherRollAfterAStrike() {
        Frame frame = new Frame();

        CompletedFrameException exception = Assertions.assertThrows(CompletedFrameException.class, () -> {
            frame.addRoll(new Roll("10"));
            frame.addRoll(new Roll("3"));
        });
    }

    @Test
    public void testFramePinFallsAreCorrectlyCalculatedAfterTwoRolls() {
        Frame frame = new Frame();
        frame.addRoll(new Roll("3"));
        frame.addRoll(new Roll("4"));

        Assertions.assertEquals(7, frame.getFramePinFalls());
    }

    @Test
    public void testFrameScoreIsCorrectlyCalculatedWithAStrike() {
        Frame frame = new Frame();
        frame.addRoll(new Roll("10"));

        Assertions.assertEquals(50, frame.calculateScore(20, 10, 10));
    }

    @Test
    public void testFrameScoreIsCorrectlyCalculatedWithASpare() {
        Frame frame = new Frame();
        frame.addRoll(new Roll("5"));
        frame.addRoll(new Roll("5"));

        Assertions.assertEquals(40, frame.calculateScore(20, 10, 10));
    }

    @Test
    public void testFrameStrikeIsIdentified() {
        Frame frame = new Frame();
        frame.addRoll(new Roll("10"));

        Assertions.assertTrue(frame.isStrike());
        Assertions.assertFalse(frame.isSpare());
    }

    @Test
    public void testFrameSpareIsIdentified() {
        Frame frame = new Frame();
        frame.addRoll(new Roll("3"));
        frame.addRoll(new Roll("7"));

        Assertions.assertTrue(frame.isSpare());
        Assertions.assertFalse(frame.isStrike());
    }
}
