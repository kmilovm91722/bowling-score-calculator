package model.frame;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import util.CompletedFrameException;

public class LastFrameTest {

    @Test
    public void testAddRollIsNotPossibleOnACompletedLastFrame() {
        Frame frame = new LastFrame();
        CompletedFrameException exception = Assertions.assertThrows(CompletedFrameException.class, () -> {
            frame.addRoll(new Roll("10"));
            frame.addRoll(new Roll("4"));
            frame.addRoll(new Roll("4"));
            frame.addRoll(new Roll("4"));
        });

        Frame frame2 = new LastFrame();
        CompletedFrameException exception2 = Assertions.assertThrows(CompletedFrameException.class, () -> {
            frame2.addRoll(new Roll("4"));
            frame2.addRoll(new Roll("4"));
            frame2.addRoll(new Roll("4"));
        });
    }
}
