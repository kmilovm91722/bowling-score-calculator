package model.game;

import model.frame.Roll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import util.WrongPlayerTurnException;

public class GameTest {

    @Test
    public void testRegisterRollDoesNotAllowASecondRollAfterAStrike() {
        Game game = new Game();

        WrongPlayerTurnException exception = Assertions.assertThrows(WrongPlayerTurnException.class, () -> {
            game.registerRoll("Jeff", new Roll("10"));
            game.registerRoll("John", new Roll("5"));
            game.registerRoll("John", new Roll("3"));
            game.registerRoll("Jeff", new Roll("10"));
            game.registerRoll("Jeff", new Roll("2"));
        });
    }

    @Test
    public void testRegisterRollDoesNotAllowAThirdRoll() {
        Game game = new Game();

        WrongPlayerTurnException exception = Assertions.assertThrows(WrongPlayerTurnException.class, () -> {
            game.registerRoll("Jeff", new Roll("10"));
            game.registerRoll("John", new Roll("5"));
            game.registerRoll("John", new Roll("3"));
            game.registerRoll("Jeff", new Roll("4"));
            game.registerRoll("Jeff", new Roll("2"));
            game.registerRoll("Jeff", new Roll("2"));
        });
    }

    @Test
    public void testRegisterRollDoesNotAllowANewPlayerAfterTheFirstRound() {
        Game game = new Game();

        WrongPlayerTurnException exception = Assertions.assertThrows(WrongPlayerTurnException.class, () -> {
            game.registerRoll("Jeff", new Roll("10"));
            game.registerRoll("John", new Roll("10"));
            game.registerRoll("Jeff", new Roll("10"));
            game.registerRoll("Mike", new Roll("10"));
        });
    }
}
