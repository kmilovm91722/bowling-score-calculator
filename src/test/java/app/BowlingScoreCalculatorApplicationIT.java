package app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BowlingScoreCalculatorApplicationIT {

    private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private final PrintStream printer = new PrintStream(outputStream);

    @BeforeEach
    public void resetStream() {
        outputStream.reset();
    }

    @Test
    public void testPerfectScore() throws IOException {
        BowlingScoreCalculatorApplication.launchBowlingScoreCalculator(getResourceAbsolutePath("perfect-score.txt"), printer);
        String expectedOutput = new String(Files.readAllBytes(Paths.get(getResourceAbsolutePath("perfect-score-expected-output.txt"))));
        Assertions.assertEquals(expectedOutput, outputStream.toString());
    }

    @Test
    public void testTwoPlayerScore() throws IOException {
        BowlingScoreCalculatorApplication.launchBowlingScoreCalculator(getResourceAbsolutePath("2-player-case.txt"), printer);
        Assertions.assertEquals(readResourceFile("2-player-score-expected-output.txt"), outputStream.toString());
    }

    @Test
    public void testZeroScore() throws IOException {
        BowlingScoreCalculatorApplication.launchBowlingScoreCalculator(getResourceAbsolutePath("zero-score.txt"), printer);
        Assertions.assertEquals(readResourceFile("zero-score-expected-output.txt"), outputStream.toString());
    }

    private String readResourceFile(String fileName) throws IOException {
        return new String(Files.readAllBytes(Paths.get(getResourceAbsolutePath(fileName))));
    }

    private String getResourceAbsolutePath(String resourceFileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(resourceFileName).getFile());
        return file.getAbsolutePath();
    }
}
